package com.epam.bubnii.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;

import java.util.concurrent.TimeUnit;

public class OperaDriverManager extends AbstractDriverManager {

    private static Logger LOGGER = LogManager.getLogger(OperaDriverManager.class);

    @Override
    protected WebDriver createWebDriver() {
        LOGGER.info("Create Opera eb driver");
        webDriver = new OperaDriver(prepareOptions());
        webDriver.manage().timeouts().implicitlyWait(basicImplicitTimeout, TimeUnit.SECONDS);
        driver_pool.set(webDriver);
        return driver_pool.get();
    }

    private static OperaOptions prepareOptions() {
        OperaOptions options = new OperaOptions();
        options.addArguments("chrome.switches", "--disable-extensions");
        return options;
    }
}
