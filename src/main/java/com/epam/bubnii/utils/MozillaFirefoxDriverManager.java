package com.epam.bubnii.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.concurrent.TimeUnit;

public class MozillaFirefoxDriverManager extends AbstractDriverManager {

    private static Logger LOGGER = LogManager.getLogger(MozillaFirefoxDriverManager.class);
    private int basicImplicitTimeout = 15;

    @Override
    protected WebDriver createWebDriver() {
        LOGGER.info("Create Chrome web driver");
        webDriver = new FirefoxDriver(prepareOptions());
        webDriver.manage().timeouts().implicitlyWait(basicImplicitTimeout, TimeUnit.SECONDS);
        driver_pool.set(webDriver);
        return driver_pool.get();
    }

    private static FirefoxOptions prepareOptions(){
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("chrome.switches", "--disable-extensions");
        return options;
    }
}
