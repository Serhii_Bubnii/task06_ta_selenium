package com.epam.bubnii.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class ChromeDriverManager extends AbstractDriverManager {

    private static Logger LOGGER = LogManager.getLogger(ChromeDriverManager.class);

    @Override
    protected WebDriver createWebDriver() {
        LOGGER.info("Create Chrome web driver");
        webDriver = new ChromeDriver(prepareOptions());
        webDriver.manage().timeouts().implicitlyWait(basicImplicitTimeout, TimeUnit.SECONDS);
        driver_pool.set(webDriver);
        return driver_pool.get();
    }

    private static ChromeOptions prepareOptions(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("chrome.switches", "--disable-extensions");
        return options;
    }
}
