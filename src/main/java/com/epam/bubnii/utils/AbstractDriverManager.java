package com.epam.bubnii.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.util.Optional;
import java.util.ResourceBundle;

public abstract class AbstractDriverManager {

    private static Logger LOGGER = LogManager.getLogger(AbstractDriverManager.class);
    private static final ResourceBundle bundle = ResourceBundle.getBundle("config_web_driver");
    private static final String WEB_DRIVER_PATH = bundle.getString("WEB_DRIVER_PATH");
    private static final String WEB_DRIVER_NAME = bundle.getString("WEB_DRIVER_NAME");
    protected static ThreadLocal<WebDriver> driver_pool = new ThreadLocal<>();
    protected int basicImplicitTimeout = 15;
    protected WebDriver webDriver;

    static {
        System.setProperty(WEB_DRIVER_NAME, WEB_DRIVER_PATH);
    }

    protected abstract WebDriver createWebDriver();

    public WebDriver getWebDriver() {
        LOGGER.info("Get web driver");
        return Optional.ofNullable(driver_pool.get())
                .isPresent() ? driver_pool.get() : createWebDriver();
    }

    public void quitWebDriver() {
        LOGGER.info("Quit web driver");
        if (webDriver != null) {
            webDriver.quit();
            webDriver = null;
        }
    }
}
