package com.epam.bubnii.utils;

public enum DriverType {
    CHROME, MOZILLA, OPERA;
}
