package com.epam.bubnii.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailPage extends PageObject {

    @FindBy(xpath = "//div[text() = 'Compose']")
    private WebElement composeButton;
    @FindBy(css = "textarea[name='to']")
    private WebElement receiverTextarea;
    @FindBy(xpath = "//span[text()='Cc']")
    private WebElement receiverCopy;
    @FindBy(css = "textarea[name='cc']")
    private WebElement receiverCopyTextarea;
    @FindBy(xpath = "//span[text()='Bcc']")
    private WebElement receiverHiddenCopy;
    @FindBy(css = "textarea[name='bcc']")
    private WebElement receiverHiddenCopyTextarea;
    @FindBy(xpath = "//input[@class='aoT']")
    private WebElement subjectField;
    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    private WebElement messageField;
    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    private WebElement sendButton;

    public GmailPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void clickComposeButton() {
        this.composeButton.click();
    }

    public void fillUpReceiverTextarea(String receiver) {
        this.receiverTextarea.clear();
        this.receiverTextarea.sendKeys(receiver);
    }

    public void clickReceiverCopy() {
        this.receiverCopy.click();
    }

    public void fillUpReceiverCopyTextarea(String receiverCopy) {
        this.receiverCopyTextarea.clear();
        this.receiverCopyTextarea.sendKeys(receiverCopy);
    }

    public void clickReceiverHiddenCopy() {
        this.receiverHiddenCopy.click();
    }

    public void fillUpReceiverHiddenCopyTextarea(String receiverHiddenCopy) {
        this.receiverHiddenCopyTextarea.clear();
        this.receiverHiddenCopyTextarea.sendKeys(receiverHiddenCopy);
    }

    public void fillUpSubjectBody(String subject) {
        this.subjectField.clear();
        this.subjectField.sendKeys(subject);
    }

    public void fillUpMessageBody(String message) {
        this.messageField.clear();
        this.messageField.sendKeys(message);
    }

    public void clickSendButton() {
        this.sendButton.click();
    }

}
