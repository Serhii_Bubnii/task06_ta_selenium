package com.epam.bubnii.bo;

import com.epam.bubnii.entity.Email;
import com.epam.bubnii.po.GmailPage;
import org.openqa.selenium.WebDriver;

public class GmailBo {

    private GmailPage gmailPage;

    public GmailBo(WebDriver webDriver){
        gmailPage = new GmailPage(webDriver);
    }

    public void sendMessage(Email email){
        gmailPage.clickComposeButton();
        gmailPage.fillUpReceiverTextarea(email.getReceiver());
        gmailPage.clickReceiverCopy();
        gmailPage.fillUpReceiverCopyTextarea(email.getReceiverCopy());
        gmailPage.clickReceiverHiddenCopy();
        gmailPage.fillUpReceiverHiddenCopyTextarea(email.getReceiverHiddenCopy());
        gmailPage.fillUpSubjectBody(email.getSubject());
        gmailPage.fillUpMessageBody(email.getMessage());
        gmailPage.clickSendButton();
    }

}
