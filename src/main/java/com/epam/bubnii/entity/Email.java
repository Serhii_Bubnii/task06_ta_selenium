package com.epam.bubnii.entity;

public class Email {

    private String receiver;
    private String receiverCopy;
    private String receiverHiddenCopy;
    private String subject;
    private String message;

    public Email() {
    }

    public String getReceiver() {
        return receiver;
    }

    public Email setReceiver(String receiver) {
        this.receiver = receiver;
        return this;
    }

    public String getReceiverCopy() {
        return receiverCopy;
    }

    public Email setReceiverCopy(String receiverCopy) {
        this.receiverCopy = receiverCopy;
        return this;
    }

    public String getReceiverHiddenCopy() {
        return receiverHiddenCopy;
    }

    public Email setReceiverHiddenCopy(String receiverHiddenCopy) {
        this.receiverHiddenCopy = receiverHiddenCopy;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public Email setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Email setMessage(String message) {
        this.message = message;
        return this;
    }
}
