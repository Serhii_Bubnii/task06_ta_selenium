package com.epam.bubnii;

import com.epam.bubnii.bo.AuthorizationBo;
import com.epam.bubnii.bo.GmailBo;
import com.epam.bubnii.entity.Email;
import com.epam.bubnii.entity.User;
import com.epam.bubnii.utils.AbstractDriverManager;
import com.epam.bubnii.utils.DriverManagerFactory;
import com.epam.bubnii.utils.DriverType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.stream.Stream;

public class AuthorizationPageTest {

    protected static String USER_EMAIL_RECEIVER = "bubniy1995@gmail.com";
    protected static String USER_EMAIL_CC = "testacount963@gmail.com";
    protected static String USER_EMAIL_BCC = "testacount971@gmail.com";
    protected static WebDriver webDriver;
    protected static AbstractDriverManager driverManager;
    private AuthorizationBo authorizationBo;
    private GmailBo gmailBo;

    @DataProvider(parallel = true)
    private Iterator<Object[]> users() {
        return Stream.of(
                new Object[]{"testacount968@gmail.com", "tao1234123"},
                new Object[]{"testacount969@gmail.com", "tao1234123"},
                new Object[]{"testacount963@gmail.com", "tao1234123"},
                new Object[]{"testacount971@gmail.com", "tao1234123"}
        ).iterator();
    }


    @BeforeClass(alwaysRun = true)
    public static void setUp() {
        driverManager = DriverManagerFactory.getDriverManager(DriverType.CHROME);
        webDriver = driverManager.getWebDriver();

    }

    @Test(dataProvider = "users")
    public void testSendEmail(String userEmail, String userPassword) throws InterruptedException {
        webDriver.get("https://mail.google.com/mail");
        authorizationBo = new AuthorizationBo(webDriver);
        gmailBo = new GmailBo(webDriver);
        authorizationBo.login(new User()
                .setEmail(userEmail)
                .setPassword(userPassword));
        gmailBo.sendMessage(new Email()
                .setReceiver(USER_EMAIL_RECEIVER)
                .setReceiverCopy(USER_EMAIL_CC)
                .setReceiverHiddenCopy(USER_EMAIL_BCC)
                .setSubject("EPAM")
                .setMessage("ky-ky"));
    }

    @AfterClass
    public static void tearDown() {
        webDriver.close();
    }

}
